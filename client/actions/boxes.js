import { getBoxesApi, getUserBoxesApi, addBoxApi, updateBoxApi, deleteBoxApi } from '../apis/boxes'
import { errMessage } from './workflow'
export const SAVE_BOXES = 'SAVE_BOXES'
export const ADD_BOX = 'ADD_BOX'
export const UPDATE_BOX = 'UPDATE_BOX'
export const DELETE_BOX = 'DELETE_BOX'
export const LOADING = 'LOADING'

// -------action creater----------
export function saveBoxes (boxes) {
  return {
    type: SAVE_BOXES,
    boxes
  }
}

export function addBox (box) {
  return {
    type: ADD_BOX,
    box
  }
}

export function updateBox (box) {
  return {
    type: UPDATE_BOX,
    box
  }
}

export function deleteBox (box) {
  return {
    type: DELETE_BOX,
    box
  }
}

// -----------thunks-----------------

// for public
export function loadBoxesThunk () {
  return (dispatch) => {
    // dispatch(loading())
    getBoxesApi()
      .then((result) => {
        dispatch(saveBoxes(result))
        return null
      })
      .catch(err => {
        dispatch(errMessage(err.message))
      })
  }
}
// for logged user
export function loadUserBoxesThunk (email) {
  return (dispatch) => {
    // dispatch(loading())
    getUserBoxesApi(email)
      .then((result) => {
        dispatch(saveBoxes(result))
        return null
      })
      .catch(err => {
        dispatch(errMessage(err.message))
      })
  }
}

export function addBoxThunk (box) {
  return (dispatch) => {
    addBoxApi(box)
      .then(result => {
        if (result.insertedId) {
          box._id = result.insertedId
          dispatch(addBox(box))
        }
        return null
      })
      .catch(err => {
        dispatch(errMessage(err.message))
      })
  }
}

export function updateBoxThunk (box) {
  return (dispatch) => {
    updateBoxApi(box)
      .then(result => {
        dispatch(updateBox(box))
        return null
      })
      .catch(err => {
        dispatch(errMessage(err.message))
      })
  }
}

export function deleteBoxThunk (box) {
  return (dispatch) => {
    deleteBoxApi(box)
      .then(result => {
        dispatch(deleteBox(box))
        return null
      })
      .catch(err => {
        dispatch(errMessage(err.message))
      })
  }
}
