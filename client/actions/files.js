import { uploadFileApi, deleteFileApi } from '../apis/files'
import { updateBoxThunk } from './boxes'
import { errMessage, selectedBox } from './workflow'
export const UPLOAD_FILE = 'UPLOAD_FILE'

// -------actions-------------
// const uploadFile = (filename) => {
//   return {
//     type: UPLOAD_FILE,
//     filename
//   }
// }

// ---------thunks---------------

export const uploadFileThunk = (formModel, box, filename) => {
  return (dispatch) => {
    // dispatch(loading())
    uploadFileApi(formModel)
      .then((result) => {
        if (result === true) {
          box.filename = filename
          alert('Uploading success!')
          dispatch(updateBoxThunk(box))
          dispatch(selectedBox(null))
          dispatch(selectedBox(box))
        } else {
          alert('Unsuccessful uploading :(')
        }
        return null
      })
      .catch(err => {
        dispatch(errMessage(err.message))
      })
  }
}

export const deleteFileThunk = (filemodel, box) => {
  return (dispatch) => {
    deleteFileApi(filemodel)
      .then(response => {
        if (response === 'true') {
          box.filename = ''
          dispatch(updateBoxThunk(box))
          dispatch(selectedBox(null))
          dispatch(selectedBox(box))
          alert('delete success!')
        } else {
          alert('file not exist')
        }
        return null
      })
      .catch(err => {
        dispatch(errMessage(err.message))
      })
  }
}
