import { loginApi, signUpApi } from '../apis/users'
import { loadUserBoxesThunk } from './boxes'
import { errMessage } from './workflow'
export const LOGGING = 'LOGGING'
export const LOGGED_IN = 'LOGGED_IN'
export const SIGN_UP = 'SIGN_UP'
export const SIGN_IN = 'SIGN_IN'
export const SAVE_USER = 'SAVE_USER'
export const LOGGED_OUT = 'LOGGED_OUT'

// ------actions---------
export const logging = () => {
  return {
    type: LOGGING
  }
}

export const loggedIn = (user) => {
  return {
    type: LOGGED_IN,
    user
  }
}

export const loggedOut = () => {
  return {
    type: LOGGED_OUT
  }
}

export const signUp = (userInfo) => {
  return {
    type: SIGN_UP,
    userInfo
  }
}

export const signIn = (userInfo) => {
  return {
    type: SIGN_IN,
    userInfo
  }
}

export const saveUser = (user) => {
  return {
    type: SAVE_USER,
    user
  }
}

// ------thunk-------------
export const signUpThunk = (User) => {
  return (dispatch) => {
    // dispatch(logging())
    signUpApi(User)
      .then(response => {
        if (response.token) {
          localStorage.setItem('id_token', response.token)
          dispatch(loggedIn({ email: User.email }))
          alert('Registration succeed!')
        } else {
          alert('Registration failed!')
        }
        return null
      })
      .catch(err => {
        dispatch(errMessage(err.message))
      })
  }
}

export const signInThunk = (User) => {
  return (dispatch) => {
    // dispatch(logging())
    loginApi(User)
      .then(response => {
        if (response.email) {
          dispatch(loggedIn({ email: response.email }))
          localStorage.setItem('id_token', response.token)
          dispatch(loadUserBoxesThunk(response.email))
        } else {
          alert('Login not successful, please try again!')
        }
        return null
      })
      .catch(err => {
        dispatch(errMessage(err.message))
      })
  }
}
