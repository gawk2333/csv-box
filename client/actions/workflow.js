export const SELECTED_BOX = 'SELECTED_BOX'
export const ERROR = 'ERROR'

export function errMessage (message) {
  return {
    type: ERROR,
    message
  }
}

export function selectedBox (box) {
  return {
    type: SELECTED_BOX,
    box
  }
}
