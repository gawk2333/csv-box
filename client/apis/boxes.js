import request from 'superagent'
const url = '/api/v1/boxes'

// for public
export const getBoxesApi = () => {
  return request.get(url)
    .then(response => {
      return response.body
    })
}
// for logged user
export const getUserBoxesApi = (email) => {
  return request.get(`${url}/${email}`)
    .then(response => {
      return response.body
    })
}

export const addBoxApi = (box) => {
  return request.post(url)
    .send(box)
    .then(response => {
      return response.body
    })
}

export const updateBoxApi = (box) => {
  return request.patch(url)
    .send(box)
    .then(response => {
      return response.body
    })
}

export const deleteBoxApi = (box) => {
  return request.delete(url + `/${box._id}`)
    .then(response => {
      return response.body
    })
}
