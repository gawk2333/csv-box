import request from 'superagent'
import FileSaver from 'file-saver'

const url = '/api/v1/files'

export const uploadFileApi = (file) => {
  return request.post(url)
    .send(file)
    .then((response) => {
      if (response.text === 'uploading success!') {
        return true
      }
      return false
    })
}

export const readFileApi = (filemodel) => {
  return request.get(url)
    .query(filemodel)
    .responseType('blob')
    .then(response => {
      return response.body
    })
    .catch(err => console.log(err))
}

export const downloadFileApi = (filemodel) => {
  return request.get(url)
    .query(filemodel)
    .responseType('blob')
    .then(response => {
      FileSaver.saveAs(response.body, filemodel.filename)
      return null
    })
    .then(() => {
      alert('download completed!')
      return null
    })
    .catch(err => console.log(err))
}

export const deleteFileApi = (file) => {
  return request.delete(url)
    .query(file)
    .then(response => {
      return response.text
    })
    .catch(err => console.log(err))
}
