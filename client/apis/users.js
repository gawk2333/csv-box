import request from 'superagent'

const url = '/api/v1/users'

export const signUpApi = (userInfo) => {
  return request.post(`${url}/register`)
    .send(userInfo)
    .then(response => {
      return response.body
    })
}

export const loginApi = (loginInfo) => {
  return request.post(`${url}/login`)
    .send(loginInfo)
    .then(response => {
      return response.body
    })
}
