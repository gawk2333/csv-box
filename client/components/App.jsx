import React, { Fragment, useEffect } from 'react'
import { connect } from 'react-redux'
import Headerr from './partial/Headerr'
import Body from './partial/Body'
import jwt from 'jwt-decode'
import { saveUser } from '../actions/user'

// import SignUpPage from './auth/SignUpPage'
import {
  loadBoxesThunk,
  loadUserBoxesThunk
} from '../actions/boxes'
const token = localStorage.getItem('id_token')

const App = ({ dispatch }) => {
  useEffect(() => {
    if (token) {
      const user = jwt(token)
      const timeStamp = Date.now()
      // console.log('user', user.exp)
      // console.log('timeStamp', timeStamp)
      if (user.exp * 1000 > timeStamp) {
        const currentUser = {
          email: user.email,
          userid: user.userid
        }
        dispatch(saveUser(currentUser))
        dispatch(loadUserBoxesThunk(currentUser.email))
      }
    } else {
      dispatch(loadBoxesThunk())
    }
  }, [])
  return (
    <Fragment>
      <Headerr/>
      <Body/>
    </Fragment>

  )
}

// function mapStateToProps (state) {
//   return {
//     user: state.user
//   }
// }

export default connect()(App)
