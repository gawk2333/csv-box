import React from 'react'
import SignInPage from './SignInPage'
import SignUpPage from './SignUpPage'
import { loggedOut } from '../../actions/user'
import { connect } from 'react-redux'
import { Button } from 'semantic-ui-react'
import { loadBoxesThunk } from '../../actions/boxes'

const AuthenticationButton = ({ user, dispatch }) => {
  const logoutHandler = () => {
    localStorage.removeItem('id_token')
    dispatch(loadBoxesThunk())
    dispatch(loggedOut())
  }

  return (
    <div className='authbtn' style={{ float: 'right' }}>
      {!user
        ? <div>
          <SignInPage />
          <SignUpPage />
        </div>
        : <Button className='logout' onClick={logoutHandler}>Log Off</Button>}
    </div>)
}

function mapStateToProps (state) {
  return {
    user: state.user
  }
}

export default connect(mapStateToProps)(AuthenticationButton)
