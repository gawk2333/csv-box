import React, { useState } from 'react'
import { connect } from 'react-redux'
import { Form, Button, Modal } from 'semantic-ui-react'
import { signInThunk } from '../../actions/user'

function SignInPage ({ dispatch }) {
  const [open, setOpen] = React.useState(false)
  const [signInForm, setsignInForm] = useState(
    {
      email: '',
      password: ''
    }
  )
  const signInHandler = () => {
    if (signInForm.email && signInForm.password) {
      dispatch(signInThunk(signInForm))
      setOpen(false)
    } else {
      alert('Some area is blank')
    }
  }
  return (
    <Modal
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
      open={open}
      trigger={<Button className='signin'>Sign In</Button>}>
      <Modal.Content>
        <Form>
          <Form.Group unstackable widths={2}>
            <Form.Input label='Email' placeholder='Address'
              onChange={(e) => setsignInForm({
                email: e.target.value,
                password: signInForm.password
              })}
            />
            <Form.Input label='PassWord' placeholder='Password' type='password'
              onChange={(e) => setsignInForm({
                email: signInForm.email,
                password: e.target.value
              })}
            />
          </Form.Group>
          <Button onClick={signInHandler} primary>Sign In</Button>
        </Form>
      </Modal.Content>
    </Modal>
  )
}

export default connect()(SignInPage)
