import React, { useState } from 'react'
import { connect } from 'react-redux'
import { Form, Button, Modal } from 'semantic-ui-react'
import { signUpThunk } from '../../actions/user'

function SignUpPage ({ dispatch }) {
  const [open, setOpen] = React.useState(false)
  const [signUpForm, setSignUpForm] = useState(
    {
      firstname: '',
      lastname: '',
      email: '',
      password: ''
    }
  )
  const signUpHandler = () => {
    if (signUpForm.firstname && signUpForm.lastname && signUpForm.email && signUpForm.password) {
      dispatch(signUpThunk(signUpForm))
      setOpen(false)
    } else {
      alert('Some area is blank')
    }
  }
  return (
    <Modal
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
      open={open}
      trigger={<Button className='signup'>Sign Up</Button>}>
      <Modal.Content>
        <Form>
          <Form.Group unstackable widths={2}>
            <Form.Input label='First name' placeholder='First name'
              onChange={(e) => setSignUpForm({
                firstname: e.target.value,
                lastname: signUpForm.lastname,
                email: signUpForm.email,
                password: signUpForm.password
              })}
            />
            <Form.Input label='Last name' placeholder='Last name'
              onChange={(e) => setSignUpForm({
                firstname: signUpForm.firstname,
                lastname: e.target.value,
                email: signUpForm.email,
                password: signUpForm.password
              })}
            />
          </Form.Group>
          <Form.Group widths={2}>
            <Form.Input label='Email' placeholder='Address'
              onChange={(e) => setSignUpForm({
                firstname: signUpForm.firstname,
                lastname: signUpForm.lastname,
                email: e.target.value,
                password: signUpForm.password
              })}
            />
            <Form.Input label='PassWord' placeholder='Phone' type='password'
              onChange={(e) => setSignUpForm({
                firstname: signUpForm.firstname,
                lastname: signUpForm.lastname,
                email: signUpForm.email,
                password: e.target.value
              })}
            />
          </Form.Group>
          <Button onClick={signUpHandler}>Sign Up</Button>
        </Form>
      </Modal.Content>
    </Modal>
  )
}

export default connect()(SignUpPage)
