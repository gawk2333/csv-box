import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { Form, Button, Item, Label, Input } from 'semantic-ui-react'
import { addBoxThunk } from '../../actions/boxes'

function AddBox ({ dispatch, setAdding, user }) {
  useEffect(() => {
    if (user) {
      newBox.boxtype = 'Private'
    } else {
      newBox.boxtype = 'Public'
    }
  }, [])
  const boxmodel = {
    _id: '',
    name: '',
    description: '',
    filename: '',
    boxtype: '',
    author: '',
    lastedited: null
  }

  const [newBox, setNewBox] = useState({
    boxmodel
  })

  const addBoxHandler = () => {
    if (newBox.name && newBox.description && newBox.boxtype) {
      if (user) {
        newBox.author = user.email
      }
      newBox.lastedited = Date.now()
      dispatch(addBoxThunk(newBox))
      setAdding(false)
    } else {
      alert('You have to fill up the form before submit.')
    }
  }
  return (
    <Item>
      <Form>
        <Form.Field>
          <Label>Name</Label>
          <Input placeholder='Name'
            onChange={(e) =>
              setNewBox({ name: e.target.value, description: newBox.description, filename: '', boxtype: newBox.boxtype })}
          />
        </Form.Field>
        <Form.Field>
          <Label>description</Label>
          <Input placeholder='Description'
            onChange={(e) =>
              setNewBox({ name: newBox.name, description: e.target.value, filename: '', boxtype: newBox.boxtype })}
          />
        </Form.Field>
        <Form.Field>
          <select
            placeholder='Select box type'
            value = {newBox.boxtype}
            onChange={(e) => {
              setNewBox({ name: newBox.name, description: newBox.description, filename: '', boxtype: e.target.value })
            }}
          >
            {user ? <option value={'Private'}>Private</option> : null }
            <option value={'Public'}>Public</option>
          </select>
        </Form.Field>
        <Button type='submit' onClick={addBoxHandler}>Submit</Button>
      </Form>
    </Item>
  )
}

function mapStateToProps (state) {
  return {
    user: state.user
  }
}

export default connect(mapStateToProps)(AddBox)
