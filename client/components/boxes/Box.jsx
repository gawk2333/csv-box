import React, { Fragment, useState } from 'react'
import { Item, Image, Button } from 'semantic-ui-react'
import { selectedBox } from '../../actions/workflow'
import { connect } from 'react-redux'
import EditBox from './EditBox'
import { deleteBoxThunk } from '../../actions/boxes'

function Box ({ box, dispatch }) {
  const [Editing, setEditing] = useState(false)

  const setSelectedBoxHandler = () => {
    dispatch(selectedBox(box))
  }
  const deleteBoxHandler = () => {
    dispatch(deleteBoxThunk(box))
  }
  const createDateTimeString = (timestamp) => {
    const date = new Date(timestamp)
    return date.toDateString() + ',' + date.toLocaleTimeString()
  }

  return (
    <Fragment>
      <Item key = {box._id}>
        <Image size='tiny' src='/photos/cardboard-box-icon.svg' />
        <Item.Content>
          <Item.Header as='a' onClick={setSelectedBoxHandler}>
          Name:{box.name}
          </Item.Header>
          <Item.Description>
            {box.boxtype}
          </Item.Description>
          <Item.Extra as='p'>
            {createDateTimeString(box.lastedited)}
          </Item.Extra>
        </Item.Content>
        <Item.Extra>
          {!Editing ? <Button basic onClick={() => setEditing(!Editing)} style={{ width: '90px', float: 'right', margin: '5px' }}>Edit</Button> : null}
          <Button onClick={deleteBoxHandler} style={{ width: '90px', float: 'right', margin: '5px' }}>Delete</Button>
        </Item.Extra>
      </Item>
      {Editing ? <EditBox box={box} setEditing={setEditing}/> : null}
    </Fragment>
  )
}

export default connect()(Box)
