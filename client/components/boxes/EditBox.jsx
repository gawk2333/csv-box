import React, { useState } from 'react'
import { connect } from 'react-redux'
import { Form, Button, Item } from 'semantic-ui-react'
import { updateBoxThunk } from '../../actions/boxes'

function EditBox ({ dispatch, setEditing, box, user }) {
  const [newName, setNewName] = useState(box.name)
  const [newDes, setNewDes] = useState(box.description)
  const [newType, setNewType] = useState(box.boxtype)

  const editBoxHandler = () => {
    if (newName === '' || newDes === '' || newType === '') {
      alert('Some content is empty')
    } else {
      const upDateBox = {
        _id: box._id,
        name: newName,
        description: newDes,
        filename: box.filename,
        boxtype: newType,
        author: user.email,
        lastedited: Date.now()
      }
      dispatch(updateBoxThunk(upDateBox))
      setEditing(false)
    }
  }

  return (
    <Item>
      <Form>
        <Form.Field>
          <label>Name</label>
          <input placeholder='Name'
            value={newName}
            onChange={(e) => { setNewName(e.target.value) }}
          />
        </Form.Field>
        <Form.Field>
          <label>description</label>
          <input placeholder='Description'
            value={newDes}
            onChange={(e) => setNewDes(e.target.value)}
          />
          <select
            placeholder='Select box type'
            value = {newType}
            onChange={(e) => {
              setNewType(e.target.value)
            }}
          >
            {user ? <option value={'Private'}>Private</option> : null }
            <option value={'Public'}>Public</option>
          </select>
        </Form.Field>
        <Button type='submit' onClick={editBoxHandler}>Save</Button>
      </Form>
    </Item>
  )
}

function mapStateToProps (state) {
  return {
    user: state.user
  }
}

export default connect(mapStateToProps)(EditBox)
