import React, { useState } from 'react'
import { readFileApi } from '../../apis/files'
import {
  usePapaParse
} from 'react-papaparse'
import { Button, Container, Grid, Modal } from 'semantic-ui-react'
import { connect } from 'react-redux'

function CsvReader ({ selectedBox }) {
  const [CSVdata, setCSVdata] = useState(null)
  const { readString } = usePapaParse()
  const [open, setOpen] = React.useState(false)
  const fileModel = {
    fileid: selectedBox._id,
    filename: selectedBox.filename
  }
  const getCsvViewHandler = () => {
    readFileApi(fileModel)
      .then(data => {
        readString(data, {
          worker: true,
          complete: (results) => {
            setCSVdata(results.data)
          }
        })
        return null
      })
      .catch(err => alert(err.message))
  }

  return (
    <Modal
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
      open={open}
      trigger={<Button className='viewfile' onClick={getCsvViewHandler}>View</Button>}
    >
      <Modal.Header>{selectedBox.filename}</Modal.Header>
      <Modal.Content image>
        <Container>
          <Grid>
            {
              CSVdata?.map((row, index) => {
                return (
                  <Grid.Row key={index} columns={row.length}>
                    {row?.map((item, index) => {
                      return (
                        <Grid.Column key={index}>
                          {item}
                        </Grid.Column>
                      )
                    })}
                  </Grid.Row>
                )
              })
            }
          </Grid>
        </Container>
      </Modal.Content>
      <Modal.Actions>
        <Button color='black' onClick={() => setOpen(false)}>
          Close
        </Button>
      </Modal.Actions>
    </Modal>)
}

function mapStateToProps (state) {
  return {
    selectedBox: state.selectedBox
  }
}

export default connect(mapStateToProps)(CsvReader)
