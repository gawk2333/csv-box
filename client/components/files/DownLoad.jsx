import React, { Fragment } from 'react'
import { downloadFileApi } from '../../apis/files'
import { deleteFileThunk } from '../../actions/files'
import { connect } from 'react-redux'
import { Button, Container, Divider } from 'semantic-ui-react'

function DownLoad ({ selectedBox, dispatch, key }) {
  // console.log(selectedBox)
  const fileModel = {
    fileid: selectedBox._id,
    filename: selectedBox.filename
  }
  const downLoadHandler = () => {
    downloadFileApi(fileModel)
  }
  const deleteHandler = () => {
    dispatch(deleteFileThunk(fileModel, selectedBox))
  }

  return (
    <Fragment>
      <Container textAlign='center'>
        <h3>{selectedBox.filename}</h3>
        <Divider/>
        <Button primary className='downloadfile' onClick={downLoadHandler}>DownLoad</Button>
        <Button secondary className='deletefile' onClick={deleteHandler}>Delete</Button>
      </Container>
    </Fragment>
  )
}

function mapStateToProps (state) {
  return {
    selectedBox: state.selectedBox
  }
}

export default connect(mapStateToProps)(DownLoad)
