import React, { useState } from 'react'
import { uploadFileThunk } from '../../actions/files'
import { connect } from 'react-redux'
import { Item, Input, Button, Label, Form } from 'semantic-ui-react'

function UpLoad ({ selectedBox, dispatch, key }) {
  const [file, setfile] = useState(null)
  const upLoadHandler = () => {
    if (file.type === 'text/csv') {
      const formData = new FormData()
      formData.append('filename', file.name)
      formData.append('_id', selectedBox._id)
      formData.append('file', file)
      dispatch(uploadFileThunk(formData, selectedBox, file.name))
    } else {
      alert('invalid upload')
    }
  }

  return (
    <Item key={key}>
      <Form>
        <Form.Field>
          <Label>Name</Label>
          <Input placeholder='Name' type='file' accept='text/csv'
            onChange={(e) => setfile(e.target.files[0])}
          />
        </Form.Field>
        <Button secondary onClick={upLoadHandler}>Upload</Button>
      </Form>
    </Item>
  )
}

function mapStateToProps (state) {
  return {
    selectedBox: state.selectedBox
  }
}

export default connect(mapStateToProps)(UpLoad)
