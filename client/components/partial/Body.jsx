import React from 'react'
import BoxContainer from './BoxContainer'
import DetailContainer from './DetailContainer'
import { Divider, Grid, Segment } from 'semantic-ui-react'

function Body () {
  return (
    <Segment placeholder style={{ height: '90vh' }}>
      <Grid columns={2} relaxed='very' stackable>
        <Grid.Column>
          <BoxContainer />
        </Grid.Column>
        <Grid.Column verticalAlign='middle'>
          <DetailContainer/>
        </Grid.Column>
      </Grid>
      <Divider vertical/>
    </Segment>
  )
}

export default Body
