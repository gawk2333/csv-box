import React, { Fragment, useState } from 'react'
import { Item, Button, Divider } from 'semantic-ui-react'
import { connect } from 'react-redux'
import Box from '../boxes/Box'
import AddBox from '../boxes/AddBox'

function BoxContainer ({ boxes }) {
  const [Adding, setAdding] = useState(false)
  return (
    <Fragment>
      {Adding ? <AddBox setAdding={setAdding}/> : null }
      <Button primary onClick={() => setAdding(!Adding)}>
        {!Adding ? <p>Add Box</p> : <p>Cancel</p>}
      </Button>
      <Divider/>
      <Item.Group divided>
        {boxes.map(box => {
          return (
            <Box key={box._id} box={box}/>
          )
        })

        }
      </Item.Group>
    </Fragment>
  )
}

function mapStateToProps (state) {
  return {
    boxes: state.boxes
  }
}

export default connect(mapStateToProps)(BoxContainer)
