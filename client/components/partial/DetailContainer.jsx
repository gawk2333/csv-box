import React, { Fragment } from 'react'
import UpLoad from '../files/UpLoad'
import DownLoad from '../files/DownLoad'
import { connect } from 'react-redux'
import { Container, Divider } from 'semantic-ui-react'
import CsvReader from '../files/CsvReader'

function DetailContainer ({ selectedBox }) {
  if (selectedBox) {
    return (
      <Container>
        Description:{selectedBox.description}
        <Divider/>
        {selectedBox.filename === ''
          ? <UpLoad/>
          : <Fragment>
            <DownLoad/>
            <CsvReader/>
          </Fragment>
        }
      </Container>
    )
  } else {
    return (
      <Fragment>
        <Container>
        </Container>
      </Fragment>
    )
  }
}

function mapStateToProps (state) {
  return {
    selectedBox: state.selectedBox
  }
}

export default connect(mapStateToProps)(DetailContainer)
