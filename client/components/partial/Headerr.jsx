import React from 'react'
import { Segment, Header } from 'semantic-ui-react'
import AuthenticationButton from '../auth/AuthenticationButton'

function Headerr () {
  return (
    <Segment basic inverted>
      <Header as='h1'>
        CSV-BOX      <AuthenticationButton/>
      </Header>
    </Segment>
  )
}

export default Headerr
