import { SAVE_BOXES, ADD_BOX, UPDATE_BOX, DELETE_BOX } from '../actions/boxes'

function reducer (state = [], action) {
  switch (action.type) {
    case SAVE_BOXES:
      return action.boxes.sort((a, b) => a.name - b.name)
    case ADD_BOX:
      return [...state, action.box].sort((a, b) => a.name - b.name)
    case UPDATE_BOX:
      if (state.some(box => box._id === action.box._id)) {
        const newState1 = state.filter(box => box._id !== action.box._id)
        const newState2 = [...newState1, action.box]
        return newState2.sort((a, b) => a.name - b.name)
      }
      return state.sort((a, b) => a.name - b.name)
    case DELETE_BOX:
      return state.filter(box => box.name !== action.box.name)
    default:
      return state
  }
}

export default reducer
