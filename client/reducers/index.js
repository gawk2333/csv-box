import { combineReducers } from 'redux'
import boxes from './boxes'
import selectedBox from './selectedbox'
import user from './user'

export default combineReducers({
  boxes,
  selectedBox,
  user
})
