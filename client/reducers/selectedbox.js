import { SELECTED_BOX } from '../actions/workflow'

function reducer (state = null, action) {
  switch (action.type) {
    case SELECTED_BOX:
      return action.box
    default:
      return state
  }
}

export default reducer
