import { LOGGED_IN, LOGGED_OUT, SAVE_USER } from '../actions/user'

function reducer (state = null, action) {
  switch (action.type) {
    case SAVE_USER:
    case LOGGED_IN:
      return action.user

    case LOGGED_OUT:
      return null
    default:
      return state
  }
}

export default reducer
