const express = require('express')
const router = express.Router()
const assert = require('assert')
const conn = require('./db')
const ObjectId = require('mongodb').ObjectId
// const auth = require('../middleware/auth')

// public user
router.get('/', (req, res) => {
  const boxes = []
  conn.db.collection('boxes').find({ boxtype: 'Public' })
    .each((err, box) => {
      assert.equal(null, err)
      if (!box) {
        res.send(boxes)
        return
      }
      boxes.push(box)
    })
})

// logged user
router.get('/:email', (req, res) => {
  const email = req.params.email
  const boxes = []
  conn.db.collection('boxes').find({ $or: [{ boxtype: 'Public' }, { author: email }] })
    .each((err, box) => {
      assert.equal(null, err)
      if (!box) {
        res.send(boxes)
        return
      }
      boxes.push(box)
    })
})

router.post('/', (req, res) => {
  conn.db.collection('boxes').insertOne(req.body)
    .then(result => {
      res.send(result)
      return null
    })
    .catch(err => console.log(err))
})

router.patch('/', (req, res) => {
  const box = req.body
  const updatedBox = {
    name: box.name,
    description: box.description,
    filename: box.filename,
    boxtype: box.boxtype,
    author: box.author,
    lastedited: box.lastedited
  }
  conn.db.collection('boxes').findOneAndReplace({ _id: ObjectId(box._id) }, updatedBox)
    .then(result => {
      res.send(result.lastErrorObject.updatedExisting)
      return null
    })
    .catch(err => console.log(err))
})

router.delete('/:boxid', (req, res) => {
  const boxid = req.params.boxid
  conn.db.collection('boxes').deleteOne({ _id: ObjectId(boxid) })
    .then(result => {
      res.send(result)
      return null
    })
    .catch(err => console.log(err))
})

module.exports = router
