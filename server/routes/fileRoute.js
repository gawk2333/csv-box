const express = require('express')
const mongoose = require('mongoose')
const multer = require('multer')
const GridFsStorage = require('multer-gridfs-storage')
const Grid = require('gridfs-stream')
const bodyParser = require('body-parser')
// const methodOverride = require('method-override')
// const assert = require('assert')
const router = express.Router()
router.use(bodyParser.json())
const conn = require('./db')
const mongoURI = require('../config').mongoURI
const ObjectId = require('mongodb').ObjectId
// Init gfs
let gfs

conn.once('open', () => {
  // Init stream
  gfs = Grid(conn.db, mongoose.mongo)
  gfs.collection('uploads')
})

// Create storage engine
const storage = new GridFsStorage({
  url: mongoURI,
  file: (req, file) => {
    return new Promise((resolve) => {
      const userfile = ObjectId(req.body._id) + req.body.filename
      const fileInfo = {
        filename: userfile,
        bucketName: 'uploads'
      }
      resolve(fileInfo)
    })
  }
})
const upload = multer({ storage })

// @route GET /
// @desc Loads form
// router.get('/', (req, res) => {
//   gfs.files.find().toArray((err, files) => {
//     // Check if files
//     if (!files || files.length === 0) {
//       res.render('index', { files: false })
//     } else {
//       files.map(file => {
//         if (
//           file.contentType === 'image/jpeg' ||
//           file.contentType === 'image/png'
//         ) {
//           file.isImage = true
//         } else {
//           file.isImage = false
//         }
//       })
//       if (err) {
//         return res.status(404).json({ err: err })
//       }
//       res.render('index', { files: files })
//     }
//   })
// })

// @route POST /upload
// @desc  Uploads file to DB
router.post('/', upload.single('file'), (req, res) => {
  res.send('uploading success!')
})

// @route GET /files
// @desc  Display all files in JSON
router.get('/files', (req, res) => {
  gfs.files.find().toArray((err, files) => {
    // Check if files
    if (!files || files.length === 0) {
      return res.status(404).json({
        err: 'No files exist'
      })
    }
    if (err) {
      return res.status(404).json({ err: err })
    }
    // Files exist
    return res.json(files)
  })
})

// @route GET /csv
// @desc Display CSV
router.get('/csv', (req, res) => {
  const filename = (ObjectId(req.query.fileid) + (req.query.filename)).toString()
  gfs.files.findOne({ filename: filename }, (err, file) => {
    // Check if file
    if (!file || file.length === 0) {
      return res.status(404).json({
        err: 'No file exists'
      })
    }
    if (err) {
      return res.status(404).json({ err: err })
    }
    // Check if image
    if (file.contentType === 'text/csv') {
      // Read output to browser
      const readstream = gfs.createReadStream(file.filename)
      readstream.pipe(res)
    } else {
      res.status(404).json({
        err: 'Not a csv'
      })
    }
  })
})

// @route DELETE /files/:id
// @desc  Delete file
router.delete('/', (req, res) => {
  const filename = (ObjectId(req.query.fileid) + (req.query.filename)).toString()
  gfs.exist({ filename: filename, root: 'uploads' }, (err, found) => {
    if (err) {
      res.send('Error on the database looking for the file.')
    }
    if (found) {
      gfs.remove({ filename: filename, root: 'uploads' }, (err, gridStore) => {
        if (err) {
          return res.status(404).json({ err: err })
        } else {
          return res.send(true)
        }
      })
    } else {
      return res.send('no file exist')
    }
  })
})

// downloading
router.get('/', (req, res) => {
  let contentType
  const filename = (ObjectId(req.query.fileid) + (req.query.filename)).toString()
  gfs.files.findOne({ filename: filename })
    .then((file) => {
      contentType = file.contentType
      // setting response header
      res.set({
        'Accept-Ranges': 'bytes',
        'Content-Disposition': `attachment; filename=${filename}`,
        'Content-Type': `${contentType}`
      })
      const readstream = gfs.createReadStream({ filename: filename })
      readstream.pipe(res)
      return null
    })
    .catch(err =>
      res.json({
        message: err.message,
        error: err
      }))
})

module.exports = router
