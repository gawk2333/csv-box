const path = require('path')
const express = require('express')
require('dotenv').config()
const bodyParser = require('body-parser')
const methodOverride = require('method-override')
// const cors = require('cors')
const server = express()
server.use(methodOverride('_method'))
server.use(express.json())
server.use(bodyParser.json())
server.use(express.static(path.join(__dirname, './public')))
// server.use(cors('*'))

const fileRoute = require('./routes/fileRoute')
const boxRoute = require('./routes/boxRoute')
const userRoute = require('./routes/userRoute')

server.use('/api/v1/files', fileRoute)
server.use('/api/v1/boxes', boxRoute)
server.use('/api/v1/users', userRoute)

module.exports = server
